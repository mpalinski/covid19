# Structure
- 0_data_cleaning.ipynb: collect, clean and merge datasets for Italy, Germany, Netherlands
    - COVID-19 cases at regional level (nuts3)
    - population density (Eurostat)
-  1_covid_density_corr.ipynb: correlate number of COVID-19 cases (as of 2020-04-01) with population density (2018)
-  1_covid_density_corr.html: interactive plotly charts (works locally)